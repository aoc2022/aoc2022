use std::fs;
use std::collections::VecDeque;
use std::collections::HashSet;

const INPUT: &str = "input.txt";
// const INPUT: &str = "ex1.txt";

fn main() {

    let input = fs::read_to_string(INPUT)
        .expect("Error reading input file ;__;");

    let mut p1_score = 0;
    let mut p2_score = 0;

    for line in input.lines() {
        p1_score = find_unique_marker(line, 4);
        p2_score = find_unique_marker(line, 14);
    }

    println!("p1 {p1_score}");
    println!("p2 {p2_score}");
}

fn find_unique_marker(signal: &str, width: usize) -> u32 {
    let mut deq = VecDeque::with_capacity(width + 1);
    for (i, c) in signal.chars().enumerate() {
        deq.push_front(c);
        if deq.len() == width + 1 {
            deq.pop_back();
            if check_unique(deq.clone(), width) {
                return i as u32 + 1;
            }
        }
    }
    0
}

fn check_unique(deq: VecDeque<char>, width: usize) -> bool {
    let mut hs: HashSet<&char> = HashSet::new();
    for c in deq.iter() {
        hs.insert(c);
    }
    if hs.len() == width { return true; }
    false
}

