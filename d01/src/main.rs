use std::fs;

const INPUT: &str = "input.txt";
// const INPUT: &str = "ex1.txt";

fn main() {

    let input = fs::read_to_string(INPUT)
        .expect("couldn't read input file");

    let mut acc: u32 = 0;
    let mut calories = 0;
    let mut top3: [u32; 3] = [0; 3];

    let input = [&input, "\n"].concat();

    for line in input.lines() {
        // if line is blank, new elf, check accumulator
        if line.eq("") {
            if acc > calories {
                calories = acc;
            }

            for (i, d) in top3.iter().enumerate() {
                if d > &acc {
                   continue
                } else {
                    top3[i] = acc;
                    break;
                }
            }
            top3.sort();
            acc = 0;
        } else {
            // otherwise, convert to digits and add to accumulator
            let num:u32 = line.parse().unwrap();
            acc = acc + num;
        }
    }
    let top3:u32 = top3.iter().sum();
    println!("p1 {calories}");
    println!("p2 {top3}");
}
