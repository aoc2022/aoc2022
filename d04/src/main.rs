use std::fs;

const INPUT: &str = "input.txt";
// const INPUT: &str = "ex1.txt";

fn main() {

    let input = fs::read_to_string(INPUT)
        .expect("Error reading input file ;__;");

    let mut p1_score = 0;
    let mut p2_score = 0;

    for line in input.lines() {
        p1_score += full_overlaps(line);
        p2_score += any_overlaps(line);
    }

    println!("p1 {p1_score}");
    println!("p2 {p2_score}");
}

fn full_overlaps(ranges: &str) -> u32 {
    let (min1, max1, min2, max2) = parse_ints(ranges);
    if min1 >= min2 && max1 <= max2 { return 1 }
    else
    if min1 <= min2 && max1 >= max2 { return 1 }
    0
}

fn any_overlaps(ranges: &str) -> u32 {
    let (min1, max1, min2, max2) = parse_ints(ranges);
    if min1 < min2 && max1 < min2 { return 0 }
    else
    if min1 > min2 && min1 > max2 { return 0 }
    1
}

fn parse_ints(line: &str) -> (u32, u32, u32, u32) {
    let (range1, range2) = line.split_once(',').unwrap();
    let (min1, max1) = range1.split_once('-').unwrap();
    let (min2, max2) = range2.split_once('-').unwrap();
    let min1:u32 = min1.parse().unwrap();
    let max1:u32 = max1.parse().unwrap();
    let min2:u32 = min2.parse().unwrap();
    let max2:u32 = max2.parse().unwrap();
    return(min1, max1, min2, max2)
}
