use std::fs;
use std::collections::HashSet;

const INPUT: &str = "input.txt";
// const INPUT: &str = "ex1.txt";

fn main() {

    let input = fs::read_to_string(INPUT)
        .expect("Error reading input file ;__;");

    let mut p1_score = 0;
    let mut p2_score = 0;

    for line in input.lines() {
        p1_score += prio_score(line);
    }

    let rucks: Vec<String> = input
        .trim()
        .split('\n')
        .map(|s| s.to_string())
        .collect();
    for group in rucks.chunks(3) {
        p2_score += group_score(group);
    }

    println!("p1 {p1_score}");
    println!("p2 {p2_score}");
}

fn prio_score(items: &str) -> u32 {
    let len = items.trim().len();
    let compart1 = &items[0..len/2];
    let compart2 = &items[len/2..];

    for item in compart1.chars() {
        if compart2.find(item) != None {
        return alpha_score(item);
        }
    }
    for item in compart1.chars() {
        if compart2.find(item) != None {
            return alpha_score(item);
        }
    }

    0
}

fn group_score(group: &[String]) -> u32 {
    let mut badge: HashSet<char> = HashSet::new();
    for ch in group[0].chars() {
        if group[1].contains(ch) {
            badge.insert(ch);
        }
    }
    for ch in group[2].chars() {
        if badge.contains(&ch) {
            return alpha_score(ch);
        }
    }
    0
}

fn alpha_score(item: char) -> u32 {
    let item = item as u32;
    if item <= 91 {
        return (item - 64) + 26;
    } else {
        return item - 96;
    }
}
