use std::fs;

const INPUT: &str = "input.txt";
// const INPUT: &str = "ex1.txt";

fn main() {

    let input = fs::read_to_string(INPUT)
        .expect("Error reading input file ;__;");

    let mut p1_score = 0;
    let mut p2_score = 0;

    for line in input.lines() {
        p1_score += rps_score(line);
        p2_score += wld_score(line);
    }
    println!("p1 {p1_score}");
    println!("p2 {p2_score}");
}

fn rps_score(plays: &str) -> u32 {
    match plays {
        "A X" => 1 + 3,
        "A Y" => 2 + 6,
        "A Z" => 3 + 0,
        "B X" => 1 + 0,
        "B Y" => 2 + 3,
        "B Z" => 3 + 6,
        "C X" => 1 + 6,
        "C Y" => 2 + 0,
        "C Z" => 3 + 3,
        _ => 0,
    }
}

fn wld_score(play: &str) -> u32 {
    match play {
        "A X" => 3 + 0,
        "A Y" => 1 + 3,
        "A Z" => 2 + 6,
        "B X" => 1 + 0,
        "B Y" => 2 + 3,
        "B Z" => 3 + 6,
        "C X" => 2 + 0,
        "C Y" => 3 + 3,
        "C Z" => 1 + 6,
        _ => 0,
    }
}
