use std::fs;

// const INPUT: &str = "input.txt";
const INPUT: &str = "ex1.txt";
 
fn main() {

    let input = fs::read_to_string(INPUT)
        .expect("Error reading input file ;__;");

    let mut p1_score = "".to_string();
    let mut p2_score = "".to_string();

    let (crates, procedure) = input.split_once("\n\n").unwrap();
    let mut stacks = parse_crates(crates);
    let mut stacks2 = stacks.clone();

    for line in procedure.lines() {
        let line:Vec<&str> = line.split(' ').collect();

        let count = line[1].parse().unwrap();
        let mut from:usize = line[3].parse().unwrap();
        let mut to:usize = line[5].parse().unwrap();
        from -= 1;  // adjust for 0 based indexing
        to -= 1;

        let mut cm9001 = "".to_string();
        for _ in 0..count {
            // p1
            let c = stacks[from].pop().unwrap();
            stacks[to].push(c);

            // p2
            let c = stacks2[from].pop().unwrap();
            cm9001.push(c);
        }
        cm9001 = cm9001.chars().rev().collect();
        stacks2[to].push_str(&cm9001);
    }

    for stack in stacks.iter_mut() {
        p1_score.push(stack.chars().last().unwrap());
    }
    for stack in stacks2.iter_mut() {
        p2_score.push(stack.chars().last().unwrap());
    }
    println!("{p1_score}");
    println!("{p2_score}");
}

fn parse_crates(crates: &str) -> Vec<String> {
    let mut crates: Vec<&str> = crates.split("\n").collect();
    crates.pop(); // remove last line, the stack labels
    let mut stacks:Vec<String> = Vec::new();
    for (i, line) in crates.iter().enumerate() {
        let mut n = 0;
        for j in (1..line.chars().count()).step_by(4) {
            if i == 0 { stacks.push("".to_string()) }   // initialize the string
            stacks[n].push(line.chars().nth(j).unwrap());
            n += 1;
        }
    }
    // reverse strings so the top of each 'stack' is at the end of the string
    for stack in stacks.iter_mut() {
        *stack = stack.trim().chars().rev().collect();
    }
    stacks
}
